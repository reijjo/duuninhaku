import { Request, Response } from "express";
import { DuuniModel } from "../db/models/duuniModel";
import { Duuni } from "../utils/types";

// /
// GET
// Get all jobs
export const getAllJobs = async (
  _req: Request,
  res: Response
): Promise<void> => {
  try {
    const jobs: Duuni[] = await DuuniModel.find({});

    res.status(200).json(jobs);
  } catch (error: unknown) {
    console.error("Error fetching all jobs", error);

    res.status(500).json({ message: "Server error fetching jobs." });
  }
};

// /
// POST
// Add new job
export const addJob = async (req: Request, res: Response): Promise<void> => {
  const { haettu, firma, title, vastattu, vastaus, extra } = req.body;

  if (!haettu || !firma) {
    res.status(400).json({ message: "Hakupäivä tai firma puuttuu!" });
    return;
  }

  try {
    // console.log("haettu jne", haettu, firma, title, vastattu, vastaus, extra);
    const newJob = new DuuniModel({
      haettu,
      firma,
      title,
      vastattu,
      vastaus,
      extra,
    });

    await newJob.save();

    res.status(201).json({
      message: "Job Added!",
    });
  } catch (error: unknown) {
    console.error("Error adding job", error);
    res.status(500).json({ message: "Server error adding job." });
  }
};

// /:id
// GET
// Get one job
export const getOneJob = async (req: Request, res: Response): Promise<void> => {
  const { id } = req.params;
  console.log("id", id);

  try {
    const job = await DuuniModel.findById(id);
    console.log("job", job);

    if (!job) {
      res.status(404).json({ message: "Job not found!" });
      return;
    }
    res.status(200).json(job);
  } catch (error: unknown) {
    console.error("Error finding job", error);
  }
};

// /:id
// PUT
// Update job
export const updateJob = async (req: Request, res: Response): Promise<void> => {
  const { job } = req.body;
  const { id } = req.params;

  try {
    const updated = await DuuniModel.findByIdAndUpdate(id, job, { new: true });

    if (!updated) {
      res.status(404).json({ message: "Job not found!" });
      return;
    }

    res.status(200).json(updated);
  } catch (error: unknown) {
    console.error("Error updating job", error);
    res.status(500).json({ message: "Server error updating job." });
  }
};

// /
// GET
// Get all jobs with no response
export const eiJobs = async (_req: Request, res: Response): Promise<void> => {
  try {
    const eiDuunit: Duuni[] = await DuuniModel.find({ vastattu: null });
    res.status(200).json(eiDuunit);
  } catch (error: unknown) {
    // console.error("Error finding eivastatut", error);
    res.status(500).json({ message: "Error fetching ei vastatut." });
  }
};
