import { app } from "./app";
import { PORT } from "./utils/config";
import chalk from "chalk";

app.listen(PORT, () => {
  console.log(chalk.cyanBright(`Server running on port ${PORT}`));
});
