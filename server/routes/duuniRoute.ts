import express from "express";
import {
  addJob,
  getAllJobs,
  getOneJob,
  updateJob,
  eiJobs,
} from "../controllers/duuniController";

const jobRouter = express.Router();

jobRouter.get("/", getAllJobs);
jobRouter.post("/", addJob);
jobRouter.get("/yks/:id", getOneJob);
jobRouter.put("/yks/:id", updateJob);

jobRouter.get("/ei", eiJobs);

export default jobRouter;
