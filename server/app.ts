import express from "express";
import morgan from "morgan";
import cors from "cors";
import { startMongoDB } from "./db/connect";
import jobRouter from "./routes/duuniRoute";

const app = express();

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());
app.use("/", jobRouter);

startMongoDB();

app.get("/ping", (_req, res) => {
  console.log("someone pinged here");
  res.send("pong");
});

export { app };
