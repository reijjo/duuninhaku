import mongoose from "mongoose";
import chalk from "chalk";
import { MONGO_URI } from "../utils/config";

export const startMongoDB = async () => {
  try {
    console.log(chalk.yellowBright("..."));

    await mongoose.connect(MONGO_URI);

    console.log(chalk.yellowBright("Connected to MongoDB!"));
  } catch (error: unknown) {
    console.error("Error connecting MongoDB", error);
  }
};
