import mongoose from "mongoose";

const duuniSchema = new mongoose.Schema({
  haettu: Date,
  firma: String,
  title: String,
  vastattu: Date,
  vastaus: String,
  extra: String,
});

duuniSchema.set("toJSON", {
  transform: (_document, returnedDuuni) => {
    returnedDuuni.id = returnedDuuni._id.toString();
    delete returnedDuuni._id;
    delete returnedDuuni.__v;
  },
});

const DuuniModel = mongoose.model("Duuni", duuniSchema);

export { DuuniModel };
