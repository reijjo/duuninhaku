import {
  ChangeEvent,
  Dispatch,
  FormEvent,
  SetStateAction,
  useState,
} from "react";
import { Duuni } from "../utils/types";
import { addJob } from "../api/jobApi";

interface AddProps {
  setOpenForm: Dispatch<SetStateAction<boolean>>;
}

const Add = ({ setOpenForm }: AddProps) => {
  const [add, setAdd] = useState<Duuni>({
    haettu: new Date(),
    firma: "",
    title: "",
    vastattu: null,
    vastaus: "",
    extra: "",
  });

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setAdd({
      ...add,
      [event.target.name]: event.target.value,
    });
  };

  // const formattedDate = (date: Date | null): string => {
  //   if (date) {
  //     const format = date.toISOString().split("T")[0];
  //     console.log("format", format);
  //     return format;
  //   }
  //   return "";
  // };

  const addNewJob = async (event: FormEvent) => {
    event.preventDefault();
    try {
      await addJob(add);
      setOpenForm(false);
    } catch (error: unknown) {
      console.error("Error adding job", error);
    }
  };

  return (
    <div className="open-modal">
      <form className="form-add" onSubmit={addNewJob}>
        <h2>Add new duuni</h2>
        <div className="add-input">
          <label htmlFor="haettu">Hakupäivä</label>
          <input
            type="date"
            name="haettu"
            id="haettu"
            onChange={handleChange}
          />
        </div>
        <div className="add-input">
          <label htmlFor="firma">Firma</label>
          <input
            type="text"
            name="firma"
            id="firma"
            value={add.firma}
            onChange={handleChange}
          />
        </div>
        <div className="add-input">
          <label htmlFor="title">Titteli</label>
          <input
            type="text"
            name="title"
            id="title"
            value={add.title}
            onChange={handleChange}
          />
        </div>
        <div className="add-input">
          <label htmlFor="vastattu">Vastattu</label>
          <input
            type="date"
            name="vastattu"
            id="vastattu"
            // value={formattedDate(add.vastattu)}
            onChange={handleChange}
          />
        </div>
        <div className="add-input">
          <label htmlFor="vastaus">Vastaus</label>
          <input
            type="text"
            name="vastaus"
            id="vastaus"
            value={add.vastaus}
            onChange={handleChange}
          />
        </div>
        <div className="add-input">
          <label htmlFor="extra">Extra info</label>
          <input
            type="text"
            name="extra"
            id="extra"
            value={add.extra}
            onChange={handleChange}
          />
        </div>
        <div className="add-buttons">
          <button type="button" onClick={() => setOpenForm(false)}>
            Cancel
          </button>
          <button type="submit">Add</button>
        </div>
      </form>
    </div>
  );
};

export default Add;
