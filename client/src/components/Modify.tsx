import {
  ChangeEvent,
  Dispatch,
  FormEvent,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import { Duuni } from "../utils/types";
import { getOneJob, updateJob } from "../api/jobApi";

interface ModifyProps {
  setOpenModify: Dispatch<SetStateAction<boolean>>;
  id: string;
}

const Modify = ({ setOpenModify, id }: ModifyProps) => {
  // const [job, setJob] = useState<Duuni | undefined>();
  const [job, setJob] = useState<Duuni>();

  useEffect(() => {
    console.log("MODIFY ID", id);
    const modifyJob = async () => {
      const myJob = await getOneJob(id);
      setJob(myJob);
    };
    modifyJob();
  }, [id]);

  const formattedDate = (date: Date | null | undefined): string => {
    return date ? new Date(date).toISOString().split("T")[0] : "";
  };

  const handleJob = (event: ChangeEvent<HTMLInputElement>) => {
    setJob((prevJob) => ({
      ...prevJob!,
      [event.target.name]: event.target.value ?? "",
    }));
  };
  console.log("JON", job);

  const jobUpdate = async (event: FormEvent) => {
    event.preventDefault();
    try {
      if (job) {
        await updateJob(job);
        setOpenModify(false);
      }
    } catch (error: unknown) {
      console.error("Error updating job", error);
    }
  };

  return (
    <div className="open-modal">
      <form className="form-add" onSubmit={jobUpdate}>
        <h2>{job?.firma}</h2>
        <h3>{job?.title}</h3>
        <div className="add-input">
          <label htmlFor="haettu">Hakupäivä</label>
          <input
            type="date"
            name="haettu"
            id="haettu"
            onChange={handleJob}
            value={formattedDate(job?.haettu) || ""}
          />
        </div>
        <div className="add-input">
          <label htmlFor="firma">Firma</label>
          <input
            type="text"
            name="firma"
            id="firma"
            value={job?.firma}
            onChange={handleJob}
          />
        </div>
        <div className="add-input">
          <label htmlFor="title">Titteli</label>
          <input
            type="text"
            name="title"
            id="title"
            value={job?.title}
            onChange={handleJob}
          />
        </div>
        <div className="add-input">
          <label htmlFor="vastattu">Vastattu</label>
          <input
            type="date"
            name="vastattu"
            id="vastattu"
            value={formattedDate(job?.vastattu) || ""}
            onChange={handleJob}
          />
        </div>
        <div className="add-input">
          <label htmlFor="vastaus">Vastaus</label>
          <input
            type="text"
            name="vastaus"
            id="vastaus"
            value={job?.vastaus}
            onChange={handleJob}
          />
        </div>
        <div className="add-input">
          <label htmlFor="extra">Extra info</label>
          <input
            type="text"
            name="extra"
            id="extra"
            value={job?.extra}
            onChange={handleJob}
          />
        </div>
        <div className="add-buttons">
          <button type="button" onClick={() => setOpenModify(false)}>
            Cancel
          </button>
          <button type="submit">Save</button>
        </div>
      </form>
    </div>
  );
};

export default Modify;
