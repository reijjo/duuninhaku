import { useState } from "react";
import { Duuni } from "../utils/types";
import Modify from "../components/Modify";

interface JobProps extends Duuni {}

const Job = ({
  id,
  haettu,
  firma,
  title,
  vastattu,
  vastaus,
  extra,
}: JobProps) => {
  const [openModify, setOpenModify] = useState(false);

  // const format = (date: Date): string => {
  //   const formatted = new Date(date).toDateString();

  //   return formatted;
  // };

  const format2 = (date: Date): string => {
    const formatted = new Date(date);
    const day = formatted.getDate();
    const month = formatted.getMonth() + 1;
    const year = formatted.getFullYear();

    return `${day}/${month}/${year}`;
  };

  const muokkaa = (id: string) => {
    console.log("id", id);
    setOpenModify(true);
  };

  return (
    <>
      {openModify && <Modify setOpenModify={setOpenModify} id={String(id)} />}

      <div className="job">
        <div className="haettu">{format2(haettu)}</div>
        <div className="firma">{firma}</div>
        <div className="title">{title}</div>
        <div className="vastattu">{vastattu ? format2(vastattu) : "-"}</div>
        <div className="vastaus">{vastaus}</div>
        <div className="extra-info">{extra}</div>
        <div className="job-modify">
          <button type="button" onClick={() => muokkaa(String(id))}>
            Muokkaa
          </button>
        </div>
      </div>
    </>
  );
};

export default Job;
