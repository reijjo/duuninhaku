export interface Duuni {
  id?: string;
  haettu: Date;
  firma: string;
  title: string;
  vastattu: Date | null;
  vastaus: string;
  extra: string;
}
