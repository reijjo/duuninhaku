import { useEffect, useState } from "react";
import { Duuni } from "../utils/types";
import Add from "../components/Add";
import { eiVastatut, getAllJobs } from "../api/jobApi";
import Job from "../components/Job";

const Homepage = () => {
  const [jobs, setJobs] = useState<Duuni[]>([]);
  const [openForm, setOpenForm] = useState(false);
  const [uusimmat, setUusimmat] = useState<boolean>(false);

  const add = () => {
    setOpenForm(true);
  };

  useEffect(() => {
    const getJobs = async () => {
      const jobs = await getAllJobs();
      setJobs(jobs);
    };
    getJobs();
  }, [openForm]);

  const firmaSorted = jobs.sort((a, b) => a.firma.localeCompare(b.firma));
  const firmaUusimmat = [...jobs].sort(
    (a, b) => new Date(b.haettu).getTime() - new Date(a.haettu).getTime()
  );
  const vastattu = jobs.filter((job) => job.vastattu);
  const eiVastattu = jobs.filter((job) => !job.vastattu);
  console.log("Jobs", jobs);
  console.log("Jobs", jobs.length);
  console.log("Jobs", vastattu.length);

  const showAll = async () => {
    const kaikki = await getAllJobs();
    setJobs(kaikki);
  };

  const showEiVastatut = async () => {
    const eiVast = await eiVastatut();
    setJobs(eiVast);
  };

  const lajittelu = uusimmat ? firmaUusimmat : firmaSorted;

  return (
    <main>
      {/* Add new */}
      {openForm && <Add setOpenForm={setOpenForm} />}
      <div className="top">
        <div className="saldo">
          <h2>Vastattu: {vastattu.length}</h2>
          <h2>Ei Vastattu: {eiVastattu.length}</h2>
          <h1>Yhteensä: {jobs.length}</h1>
        </div>
        <div className="nappulat">
          <button onClick={showAll}>Kaikki</button>
          <button onClick={showEiVastatut}>Ei vastattu</button>
          <button onClick={() => setUusimmat(!uusimmat)}>
            {uusimmat ? "Aakkos" : "Uusimmat"}
          </button>
        </div>
        <div className="uusduuni">
          <button onClick={add}>Add new</button>
        </div>
      </div>
      <div className="the-grid">
        <div className="otsikot">
          <div>Haettu</div>
          <div>Firma</div>
          <div>Title</div>
          <div>Vastattu</div>
          <div>Vastaus</div>
          <div>Extra info</div>
          <div>Modify</div>
        </div>

        {lajittelu.map((job) => (
          <Job
            key={job.id}
            id={job.id}
            haettu={job.haettu}
            firma={job.firma}
            title={job.title}
            vastattu={job.vastattu}
            vastaus={job.vastaus}
            extra={job.extra}
          />
        ))}
      </div>
    </main>
  );
};

export default Homepage;
