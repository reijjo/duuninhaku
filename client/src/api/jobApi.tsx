import axios from "axios";
import { Duuni } from "../utils/types";

const baseUrl = "http://localhost:3001";

export const getAllJobs = async () => {
  const res = await axios.get(baseUrl);
  return res.data;
};

export const addJob = async (job: Duuni) => {
  const res = await axios.post(baseUrl, job);
  return res.data;
};

export const getOneJob = async (id: string) => {
  const res = await axios.get(`${baseUrl}/yks/${id}`);
  return res.data;
};

export const updateJob = async (job: Duuni) => {
  console.log("axiosjob", job);
  const res = await axios.put(`${baseUrl}/yks/${String(job.id)}`, { job });
  return res.data;
};

export const eiVastatut = async () => {
  const res = await axios.get<Duuni[]>(`${baseUrl}/ei`);
  return res.data;
};
